$(document).ready(function() {

	heap.track("Document Ready", {})

	var owl = $("#owl-demo");
	owl.trigger('owl.stop');

	owl.owlCarousel({
		autoHeight: false,
		items: 1,
		itemsDesktop: [1000, 1],
		itemsDesktopSmall: [900, 1],
		itemsTablet: [600, 1],
		itemsMobile: [500, 1]
	});

	// Custom Navigation Events
	$(".next").click(function() {
		owl.trigger('owl.next');
	})
	$(".prev").click(function() {
		owl.trigger('owl.prev');
	})

	$("body").swipe({
		swipeLeft: function(e, t, n, r, i) {
			$(this).parent().carousel("prev"), heap.track("Swipe", {
				swipe_direction: "left"
			})
		},
		swipeRight: function() {
			$(this).parent().carousel("next"), heap.track("Swipe", {
				swipe_direction: "right"
			})
		},
		threshold: 0
	})
	
	var parseAPPID = "UWW0790oKQJjqcuaDQxQ5htv5tNUDpRUyrWaLq37";
	var parseJSID = "QiwsalIT05TNMRv1MHC6am8bDzeNFOaBzoIqKI9g";
	
	Parse.initialize(parseAPPID, parseJSID);

	$(window).scroll(function(){
	  heap.track("Scroll", {})
	});
	
	$(".analytics").click(function(){
	
	  //if link
	  if ($(this).prop("tagName").toLowerCase() === "a" ) {
	    var url = $(this).prop("href")
	    heap.track("Click " + url, {})
	  } else {
	    heap.track("Click " + $(this).attr("id"), {})
	  }
	 
	})

});
