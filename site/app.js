var connect = require('connect'),
  app = connect.createServer(),
  port = process.env.PORT,
  contact = require('./scripts');

app.use(connect.static(__dirname + '/public'));
app.use(connect.compress());
contact.setEndpoints(app);

app.listen(port, function(){
  console.log('Hexo is running on port %d', port);
});