title: Frequently Asked Questions
pagetype: contractor
---
####1. **What will I do in this job?**

**Please check:** [our current open tasks](../tasks)

####2. **Do we need another website for the mobile layout?**

We need **Responsive Layout**.

####3. **When is the deadline of the project?**

We generally don't set hard deadlines but instead ask that you only take the task if you can either start the same day, or, if it is late in the day, to start the ext day. Communication is important so we ask that you keep us updated and ask questions if you run into issues..

####4. **How will I be paid?**

It would be a fixed price depending on the workload given to you. For example: a small task might be **$15** fixed rate. We are on startup so expect larger task as we move on. For contractors who we have already worked with on a task successfully, we are sometimes able to assign more than one task at a time.

####5. **Is it going to be hourly?**

We don't offer hourly rate on our new contractors as some just waste their time and not work. Eventually if you are already a trusted contractor, in the future it is possible for upcomming big projects.

####6. **Can we get a test project?**

Yes, [please refer here](/contractor/forms/contractor-quickstart).

####7. **How can we communicate?**

The only mean of communication we have is through Gtalk/Google Hangouts as well as email.

####8. **The ammount  of payment is low**

No, it is not if you understood what would be the job here. We base the fixed price on the ammount of work we give you.

####9. **Do I have to adjust to your time?**

While this could be a plus factor, so that there would be no communication delays, we don't require you to skip your sleep. If you think you can manage to resolve whatever problem you'll encounter correctly and without asking us then it's really fine not to adjust with our local time.

####10. **Where do I send finished projects?**

We will provide you with the appropriate email addresses to send your submission to.

####11. **Is this a long term job?**

Yes it can become one. We are on startup, which is the reason why we just have fixed projects as of the moment, as we are still building the foundation of the team.

####12. **Can I apply for another job posting?**

Yes, as long as it does not affect your project with us. Eventually you may not be needing another job since if we grow, our projects are bigger as well.