title: Contractor Policies
pagetype: contractor
---
<br/>
<b>Communication Channels to Communicate with Us:</b>

<a href="http://odesk.com">Odesk.com</a>
Gtalk (we will message you our Gtalk ID)
Email (we will message you which email address)
<br/>
We are a new company and currently growing and mainly have small tasks with relatively low budgets. It is our hope that we can work with contractors on a regular basis and, as we grow, we can then potentially provide those who have performed well with more long-term employment.

It is our belief that it is best to keep things simply and not have a lot of policies in place. Because of this, the ones we do have are important and we ask that you take a couple minutes to review:

- <b>Development Environment Setup:</b> Our project uses node.js as well as various other libraries. While the project is not necessarily complicated to setup, challenges can arise when setting it up on various environments. To address this, we have made available workspaces on <a href="http://c9.io">Cloud9 IDE</a> so that minimal project setup and configuration is needed. Since we put effort into maintaining these workspaces, we are, for the most part, unable to help with local environment setup of the project. If your internet connection speed is fast enough to use Cloud9, we recommend using it as it should enable less overhaed in starting work. For contractors that we work with on a regular basis, accommodations may be made to help setup an environment outside of Cloud9 if desired. Again, you do not have to use Cloud9 for work, but it is presented as what we believe is the easiest option.

- <b>Work Timings:</b> As a contractor, you set your own schedule, however, we can only work with contractors long-term who have good communication skills relating to project timelines. Most tasks are fairly small, so they do not have large turnaround times. We understand that unforseen circumstances may arise where work may be delayed, however, it is critical to communicate with us via designated communication channels as soon as you know that there may be a delay so that we can plan accordingly.

- <b>When things do not go as expected:</b> Most likely our staff has a different schedule than you do, due to timezone differences. This can make communication difficult in that there are likely to be long periods where you cannot reach us to ask questions if you run into issues during a project. We ask that if you reach a point where you feel that you cannot continue withour our input or assistance, and we are not reachable on Gtalk, that you pause work and send us an email clearly explains the issue. We will then address the issue as soon as possible and get back to you. This will prevent you from wasting time trying to resolve an issue and we do not want you to feel pressure in completing a task when there are clearly issues that we need to assist with, so this will not be held against you.

