title: Hiring for Designers
pagetype: contractor
form: contractor-quickstart
---
###*<span style="color:red">We are currently hiring for graphic designers to work with us, on a potentially long-term basis. The info below is about setting up a workspace with our framework to be able to work with us. If interested, we ask that you go through the tutorial below and then submit the form at the end with the requested information. We currently use [Odesk.com](http://odesk.com) to manage contracts as this provides protection and assurance for our contractors.</span>*
<br/>
## **Cloud9 and Hexo Project Setup Tutorial**

###*Note: Pictures are meant as a guide, please read all text first*

##1. Create an account in [cloud9.io](http://c9.io). Please also refer to [hexo.io](http://hexo.io) for information on the Hexo blog framework.

![alt text](http://s27.postimg.org/hzr6012lv/image.jpg "Step 1")


<br/><br/>
##2. Go into the dashboard.

![alt text](http://s27.postimg.org/mukuyb0xf/image.jpg "Step 2")


<br/><br/>
##3. Click create new workspace.

![alt text](http://s27.postimg.org/o1ysqir1v/image.jpg "Step 3")


<br/><br/>
##4. Put a Name for the workspace and click create.


<br/><br/>
##5. It will be processed on the left side and just wait until finished

![alt text](http://s23.postimg.org/exbm5xgi3/image.jpg "Step 4")


<br/><br/>
##6. Now click the workspace and click start editing.

![alt text](http://s23.postimg.org/d0ew1cpu3/image.jpg "Step 5")


<br/><br/>
##7. Now [upload this zip file](/files/project_start.zip)

![alt text](http://s23.postimg.org/jcu1b6swb/image.jpg "Step 6")

<br/><br/>
##8. You will now have to wait until the file is uploaded completely.

![alt text](http://s23.postimg.org/ka0gk24ej/image.jpg "Step 7")

<br/><br/>
##9. You will now open a new terminal, there are two ways to open it one is in the picture shown below and the other one is by simply pressing **alt+t**

![alt text](http://s23.postimg.org/iyspbl8sr/image.jpg "Step 8")

<br/><br/>
##10. You will now have to type in the command below to unzip the project:

	unzip project_start.zip
	

![alt text](http://s23.postimg.org/rxnd2d32j/image.jpg "Step 9")

<br/><br/>
##11. After unzipping the file. You have to input now these code in the terminal

        npm install -g hexo


<br/><br/>
##12. Now you have to go to the main directory by entering:

	cd site

![alt text](http://s16.postimg.org/hegt0u7n9/cd_hex.jpg "cd site")


<br/><br/>
##13. In the terminal, enter:

	npm install

*Please note that when using the terminal, a positive response is not always given by the terminal after executing a command, meaning it may just start a new line on the terminal. If no error message shows, this indicates that the terminal command executed successfully.*


<br/><br/>
##14. Now you have to run Hexo by entering:

	hexo server

![alt text](http://s16.postimg.org/pmiqltzjp/hexo.jpg "hexo server")

##15. There is a format on the link for the hexo file. It is <http://<cloud9-workspace-name>-<cloud9-username>.c9.io>. In this case it is http://hexoexample-paulowilson.c9.io

<br/>

###*<span style="color:red">Now, if you are still interested to work with us as a designer, please do this initial task below and then send us the project url, as well as share your cloud9 workspace with us (we will ping you our cloud9 workspace id)</span>*

**Note:** *We have different project and theme names throughout our projects so we don’t use those names, instead we use <name> as a variable placeholder for the name we assign you for your task.*

##The core of the Hexo.io framework is theming, as it is a very lightweight framework that basically just does the following:

	Takes a collection of files that are located under:

		/site/source/_posts/

	Apply a theme to them, using the theme code under:

		/site/themes/<theme-name>/

	Generate a finished static website. 

*Static means that the whole site is pre-generated, this allows us to push our site to a global content distribution network which makes it load fast on mobile devices. We also integrate front-end javascript code using the angular.js framework to make calls to a backend API hosted by Parse.com. This enables full-stack web applications that are lightweight and easy to create new projects with.*


##Make change in layout:

Hexo uses the concept of “partial” files, which are snippets of HTML / JavaScript code that define a page’s layout. 

To add a partial file:

In the file:

	/site/themes/eshopperK/layout/layout.ejs

Insert the line:

	<%- partial('_partial/hello-world.ejs', {}) %>

Create the folder:

	/site/themes/eshopperK/layout/_partial

In the _partial folder, create the file:

	/site/themes/eshopperK/layout/_partial/hello-world.ejs

Now you have created the partial file that you have referenced in:

	/site/themes/eshopperK/layout/layout.ejs

Edit the *hello-world.ejs* file, add some simple markup text:

	###Hello World!
	
##Validate the change, you should be able to figure the url using the tutorial above this example.

<br/>

###*<span style="color:red">Please submit the form once you have completed the above and if you are interested to work with us. Thank you for your interest.</span>*

<br/>
