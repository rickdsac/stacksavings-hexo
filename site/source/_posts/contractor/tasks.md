title: Tasks
pagetype: contractor
---
Not all of our tasks are listed here. We are currently have a large number of small tasks so can't always post them here. 

Please see our [Contractor Quick Start document](../forms/contractor-quick-start) for info on how you can get started working with us.pagetype: contractor

##*Current Job Openings*

*Web Designer - CSS / HTML / JavaScript*

*JavaScript full-stack developer - node.js, angular.js*