title: Hexo Advanced Theming
pagetype: contractor
---
#### Using files in Hexo .ejs theme files ( CSS, JavaScript, images, etc)

Within Hexo, any file needs to be included under a “source” directory, as this allows it to be downloaded directly. 

For .css, .js or image files related to a theme, they should be put under this directory *Note: please replace <theme-name> with the actual theme name, as it varies from project to project*

	/site/themes/<theme-name>/source

You can create sub-directories within a source folder, if they don’t already exist, so, for css, the correct directory would be:

	/site/themes/<theme-name>/source/css

The file can then be referenced with a path relative to root, so, if there is a file at this path, within the Hexo project:

	/site/themes/<theme-name>/source/css/sytel.css

It can be referenced in code like this:

	  <link rel="stylesheet" href="/css/style.css">


For files that are specific to a post, such as an image that shows on just one page of the site, they can be place in this source folder:

/site/source/mountain.jpg

An example is:

	/site/source/mountain.jpg

This image could be included anywhere in the project like this:

	<img src="/mountain.jpg”>
