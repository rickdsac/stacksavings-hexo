title: Working Hours
pagetype: contractor
---
It doesn't really matter what time you work on your project as long as it is within our timeframe before your submission is needed. However you need to be aware of the best time you can contact us or we can contact you for some matters. 

ie:
  * Interview before hire
  * Project details
  * Project malfunctions
  * Clarify project
  * QA assesment / result
  * Updates or new task to be given to you

Our client is currently in **UTC-8:00**, if you think you can manage and work on your own without our assistance, then it's fine for you to work on your desired time, however we encourage you to adjust your time to us when you think you'll gonna need assistance from us. May we also ask you to be available on the time that our client need to contact you, that time is usually from **2am to 8am UTC**, eventhough he is available from 6pm onwards UTC, we need to be available on the time that the client is usually free as this is the time that our client can communicate with us freely for any updates or if the client needs an update from us. Your cooperation will be much appreciated.

For our QA, he's currently in **UTC+8:00**. Our QA is mostly available from **4am to 7pm UTC** If in case that our QA is available and the client is not during your work hours. You can approach our QA for any matter. If your question can't be answered by our QA, he will then contact our client and will pass you over the answer as soon as he get's a response.