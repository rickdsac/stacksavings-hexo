/**
 * Replace heroku 'password' with the actual password and save it under
 * scripts/mongo_config.js.
 *
 * mongo_config.js won't be tracked by git as it was added to
 * .gitignore
 */

var extend = require('util')._extend,
    path = require('path');

var local = {URI: 'mongodb://localhost/test'}
var heroku = {URI: 'mongodb://rick:password@ds031561.mongolab.com:31561/heroku_app33201358'} 

var defaults = {
  root: path.normalize(__dirname + '/..'),
  env: process.env.NODE_ENV || 'development'
};

/**
 * Expose
 */

module.exports = {
  development: extend(local, defaults),
  production: extend(heroku, defaults)
}[process.env.NODE_ENV || 'development'];
