var bodyParser = require('body-parser'),
    mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var contactSchema = new Schema({
    name: String,
    email: String,
    message: String
});

var contact = mongoose.model('contact', contactSchema);

try {
    var config = require('./mongo_config');

    mongoose.connect(config.URI);
    mongoose.connection
        .on('error', console.error.bind(console, 'Connection error:'))
        .once('open', function callback () {
            console.log('[info] Mongoose connected to database');
        });
} catch (e) {
    console.log(e);
    console.log('Make sure you edit scripts/mongo_config_example.js and save it as scripts/mongo_config.js');
    process.exit(1);
}

if (typeof hexo !== 'undefined'){
    var filter = hexo.extend.filter;
	filter.register('server_middleware', function (app) {
		setEndpoints(app);
	});
} else
	module.exports = {setEndpoints: setEndpoints};

function setEndpoints(app){
    app.use(bodyParser.urlencoded({ extended: false }));
    
    app.use('/contact/', function(request, response){
	if (request.method === 'POST'){
	    var formBody = request.body;
	    
	    var newEntry = new contact({name: formBody['name'],
                                       email: formBody['email'],
		                        message: formBody['message']});

	    newEntry.save(function(err){
	        if (err)
		    return console.error(err);
	    });

	    response.writeHead(302, {
		'Location': '/contact/index.html'
			     // add other headers here,
			     // you could send the user to a new route where he gets a
			     // thank you message
	    });

	    response.end();
	}
    });

	app.use('/entries', function(req, res){
	    contact.find(function(err, entries){
                if (err)
		    return console.error(err);

		res.end(JSON.stringify(entries, 2, 2));
	    });
	});
};

